FROM centos:7

MAINTAINER Thorsten Kollegger <t.kollegger@gsi.de>

USER root

RUN  yum -y update && \
     yum -y install epel-release && \
     yum -y groupinstall development && \
     yum -y install python34 python34-pip python34-tkinter python34-devel && \
     pip3 install --upgrade pip && \
     pip install numpy scikit-learn Jupyter scipy ipython matplotlib \
         pandas pillow mglearn

ENV TINI_VERSION v0.14.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini

RUN useradd -m -U sklearn

USER sklearn
WORKDIR /home/sklearn
RUN mkdir /home/sklearn/work
RUN jupyter notebook --generate-config

USER root
COPY notebooks/jupyter_notebook_config.json /home/sklearn/.jupyter
RUN chown sklearn:sklearn /home/sklearn/.jupyter/jupyter_notebook_config.json
USER sklearn

EXPOSE 8888
ENTRYPOINT ["/usr/bin/tini", "--"]
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0"]
