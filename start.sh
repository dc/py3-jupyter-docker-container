#!/bin/bash
docker build -t scikit:latest .
docker run -d --rm -v $PWD:/home/sklearn/work --name scikit -p 8888:8888 scikit:latest
