Step 1: install docker

Step 2: start the docker service

Step 3: execute start.sh

Step 4: go to http://127.0.0.1:8888/ and see if it works, password is scikit

Step 5: enjoy

Step 6: to stop the container execute "docker stop scikit", next time you can start at Step 6 again... all jupyter notebooks you created should be in the directory where you executed Step 4.

